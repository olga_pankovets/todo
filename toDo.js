﻿var app = angular.module('toDoApp', []);

app.controller('taskCtrl', function ($scope, $http) {
    $scope.tasks = [];
    $http({
        method: "GET",
        url: "Home/GetCurrentTasks"
    }).then(function mySucces(response) {
        angular.forEach(response.data, function (item) {
            $scope.tasks.push(item);
        }); 
    });

    //$scope.tasks = [{ Name: "My first task", Completed: false }, { Name: "My second task", Completed: false }, { Name: "My completed task", Completed: true }];
    $scope.newTaskName = "";
    $scope.tasksFilter = 'none';

    $scope.addTask = function ($event) {
        if ($scope.newTaskName && ($event.type == 'keyup' && $event.keyCode==13) || $event.type=='click') {
            var newTask = { Name: $scope.newTaskName, Completed: false };
            $scope.tasks.push(newTask);
            $scope.newTaskName = "";
        }
    }

    $scope.deleteTask = function (task) {
        var index = $scope.tasks.indexOf(task);
        $scope.tasks.splice(index, 1);
    }

    $scope.clearCompleted = function () {
        angular.forEach($scope.tasks, function (task) {
            if (task.Completed == true) {
                var index = $scope.tasks.indexOf(task);
                $scope.tasks.splice(index, 1);
            }
        });
        return;
    }

    $scope.saveTasks = function () {
        var myData = angular.toJson($scope.tasks);
        $http.post("Home/SaveCurrentTasks", myData).then(function mySucces(response) {alert("All your tasks have been saved") });
    }

});

app.filter('completedFilter', function () {
    return function(input, filter){
        if(filter=='none')
        {
            return input;
        }
        else{
            var result = [];
            angular.forEach(input, function (task) {
                if(task.Completed==filter)
                {
                    result.push(task);
                }
            });
            return result;
        }
    };
});

