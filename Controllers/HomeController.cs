﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToDoApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetCurrentTasks()
        {
            //these could have been records from database
            var tasks = new List<object>();

            tasks.Add(new { Name = "This is my first task", Completed = false });
            tasks.Add(new { Name = "This is my second task", Completed = false });
            tasks.Add(new { Name = "This is my third task", Completed = true });

            return Json(tasks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCurrentTasks(Object tasks)
        {
            //as server part is not required, we don't parse the json object
            return new EmptyResult();
        }
    }
}